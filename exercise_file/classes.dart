class WinningApps {
  final String name_of_app;
  final String category;
  final String developer;
  final int year;

  WinningApps(this.name_of_app, this.category, this.developer, this.year);

  void trasformToUpper() {
    final upperName = name_of_app.toUpperCase();
    print(upperName);
  }
}

void main() {
  final app = WinningApps('Ambani Africa', 'Best Gaming Solution, Best Educational Solution, Best South African Solution, Overall App Of The Year Winner', 'Mukundi Lambani', 2021);
  
  print(app.name_of_app);
  print(app.category);
  print(app.developer);
  print(app.year);
  app.trasformToUpper();
}
