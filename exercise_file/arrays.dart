
void main() {
  sortedList();

  yearWinner();

  print(list_of_winners.length);
}

void sortedList() {
  list_of_winners.sort(((a, b) => (a['app']).compareTo(b['app'])));
  for (var i = 0; i < list_of_winners.length; i++) {
    print(list_of_winners[i]['app']);
  }
}

void yearWinner() {
  for (var i = 0; i < list_of_winners.length; i++) {
    if (list_of_winners[i]['year'] == 2017 ||
        list_of_winners[i]['year'] == 2018) {
      print(list_of_winners[i]['app']);
    }
  }
}

List<Map> list_of_winners = [
  {'year': 2012, 'app': 'FNB'},
  {'year': 2013, 'app': 'SnapScan'},
  {'year': 2014, 'app': 'LIVE inspect'},
  {'year': 2015, 'app': 'WumDrop'},
  {'year': 2016, 'app': 'Domestly'},
  {'year': 2017, 'app': 'Shyft'},
  {'year': 2018, 'app': 'Khula Ecosystem'},
  {'year': 2019, 'app': 'Naked Insurance'},
  {'year': 2020, 'app': 'Easy Equities'},
  {'year': 2021, 'app': 'Ambani Africa'}
];
